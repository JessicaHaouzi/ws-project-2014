package resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import twitter4j.Place;
import twitter4j.Twitter;
import twitter4j.TwitterException;

@Path("/locations")
public class LocationsResource {

	/**
	 * Show information about a certain location
	 * 
	 * @param location_id
	 *            is the location ID
	 * @return
	 */
	@GET
	@Path("/{location_id}")
	@Produces(MediaType.APPLICATION_XML)
	public String GetLocationInfo(@PathParam("location_id") String location_id) {

		UserAuthentification client = new UserAuthentification();
		Twitter userAccount = client.GetTwitterAccount();
		String response = null;
    	
    	try {
    		Place place = userAccount.placesGeo().getGeoDetails(location_id);
			response = XmlFunctions.GenerateUserLocationDetailledXml(place);
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response;
	}
}
