package resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import twitter4j.GeoLocation;
import twitter4j.QueryResult;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;

@Path("/tweets")
public class TweetsResource {

	/**
	 * Get one or several tweets
	 * 
	 * @param tweet_id
	 *            (optional) ID of the tweet to search
	 * @param query
	 *            (optional) query associated with the tweet
	 * @param count
	 *            (optional) number of tweets to return in the response
	 * @return
	 * @throws TwitterException
	 */
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_XML)
	public String SearchTweet(
			@DefaultValue("0") @QueryParam("tweet_id") long tweet_id,
			@DefaultValue("null") @QueryParam("query") String query,
			@DefaultValue("20") @QueryParam("count") int count) {

		UserAuthentification client = new UserAuthentification();
		Twitter userAccount = client.GetTwitterAccount();
		String response = null;
		
		try {
			
			if (tweet_id != 0) {
				int n = 1;
				twitter4j.Status searchStat = userAccount.showStatus(tweet_id);
				response = XmlFunctions.GenerateTweetInfo(searchStat,n);
			}
			else if (query != "null") {
				twitter4j.Query q = new twitter4j.Query();
				q.count(count);
				q.setQuery(query);
				QueryResult sr = userAccount.search(q);
				List<twitter4j.Status> statuslist = sr.getTweets();
				response = XmlFunctions.GenerateTweetsListInfo(statuslist);
			}
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * Create a new tweet
	 * 
	 * @param userID is the authenticated user' ID
	 * @param status_text
	 * @param location_id
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.APPLICATION_XML)
	public String CreateNewTweet(@FormParam("status_text") String status_text,
			@DefaultValue("0") @FormParam("location_id") String location_id,
			@DefaultValue("0") @FormParam("long") float longitude,
			@DefaultValue("0") @FormParam("lat") float latitude) {

		UserAuthentification client = new UserAuthentification();
		Twitter userAccount = client.GetTwitterAccount();
		String response = null;
		
		try {

			GeoLocation geo = new GeoLocation(latitude, longitude);
			StatusUpdate statup = new StatusUpdate(status_text);
			statup.location(geo);
			statup.placeId(location_id);
			int n = 2;

			Status status = userAccount.updateStatus(statup);
			
			response = XmlFunctions.GenerateTweetInfo(status,n);

		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * Delete an old tweet
	 * 
	 * @param userID is the authenticated user' ID
	 * @param tweet_id
	 * @return
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_XML)
	public String DeleteTweet(@QueryParam("tweet_id") long tweet_id) {

		UserAuthentification client = new UserAuthentification();
		Twitter userAccount = client.GetTwitterAccount();
		String response = null;
		

		try {
			int n = 2;
			Status status = userAccount.destroyStatus(tweet_id);
			response = XmlFunctions.GenerateTweetInfo(status,n);
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * Get the 20 most recent tweets favorited by the authenticated user
	 * 
	 * @param userID
	 *            is the authenticated user' ID
	 * @return
	 */
	@GET
	@Path("/favorites")
	@Produces(MediaType.APPLICATION_XML)
	public String GetFavoritesList() {

		UserAuthentification client = new UserAuthentification();
		Twitter userAccount = client.GetTwitterAccount();
		String response = null;

		try {
			int n = 1;
			ResponseList<Status> status = userAccount.getFavorites();
			response = XmlFunctions.GenerateTweetsListInfo(status,n);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * Add a tweet to the favorites list
	 * 
	 * @param userID
	 *            is the authenticated user' ID
	 * @param tweet_id
	 *            is the ID of the tweet to add
	 * @return
	 */
	@POST
	@Path("/favorites/{tweet_id}")
	@Produces(MediaType.APPLICATION_XML)
	public String AddFavorite(@PathParam("tweet_id") long tweet_id) {

		UserAuthentification client = new UserAuthentification();
		Twitter userAccount = client.GetTwitterAccount();
		String response = null;

		try {
			int n = 5;
			Status status = userAccount.createFavorite(tweet_id);
			response = XmlFunctions.GenerateTweetInfo(status,n);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response;
	}

	/**
	 * Remove a tweet from the favorites list
	 * 
	 * @param userID
	 *            is the authenticated user' ID
	 * @param tweet_id
	 *            is the ID of the tweet to remove
	 * @return
	 */
	@DELETE
	@Path("/favorites/{tweet_id}")
	@Produces(MediaType.APPLICATION_XML)
	public String RemoveFavorite(@PathParam("user_id") long userID,
			@PathParam("tweet_id") long tweet_id) {

		UserAuthentification client = new UserAuthentification();
		Twitter userAccount = client.GetTwitterAccount();
		String response = null;

		try {
			int n = 4;
			Status status = userAccount.destroyFavorite(tweet_id);
			response = XmlFunctions.GenerateTweetInfo(status,n);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response;
	}
}
