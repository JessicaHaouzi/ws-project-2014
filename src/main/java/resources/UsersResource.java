package resources;

import java.io.File;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * Users resource (exposed at "/users" path)
 */
@Path("/users")
public class UsersResource {
    
	/**
	 * Get general information about a user
	 * @param screenName is the screen name of the user we want
	 * @return
	 */
    @GET @Path("/{screen_name}")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserGeneralInformation(@PathParam("screen_name") String screenName) {
    	
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.showUser(screenName);
			response = XmlFunctions.GenerateUserInfoXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get settings properties of the authenticated user
     * @return
     */
    @GET @Path("/settings")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserSettings() {
    	
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;
    	
    	try {
			twitter4j.AccountSettings userSettings = userAccount.getAccountSettings();
			response = XmlFunctions.GenerateUserSettingXml(userSettings);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get time zone setting of the authenticated user
     * @return
     */
    @GET @Path("/settings/time_zone")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserTimeZone() {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;
    	
    	try {
			twitter4j.AccountSettings userSettings = userAccount.getAccountSettings();
			response = XmlFunctions.GenerateUserTimeZoneSettingXml(userSettings);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Set the time zone setting of the authenticated user
     * @param timeZone is the new time zone
     * @return
     */
    @PUT @Path("/settings/time_zone")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_XML)
    public String SetUserTimeZone(@FormParam("time_zone") String timeZone) {
    	
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;
    	
    	try {
			twitter4j.AccountSettings userSettings = userAccount.updateAccountSettings(null, null, null, null, timeZone, null);
			response = XmlFunctions.GenerateUserTimeZoneSettingXml(userSettings);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get the language setting of the authenticated user
     * @return
     */
    @GET @Path("/settings/lang")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserLang() {
    	
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;
    	
    	try {
			twitter4j.AccountSettings userSettings = userAccount.getAccountSettings();
			response = XmlFunctions.GenerateUserLangSettingXml(userSettings);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Set the language setting of the authenticated user
     * @param lang is the new language
     * @return
     */
    @PUT @Path("/settings/lang")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_XML)
    public String SetUserLang(@FormParam("lang") String lang) {
    	
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;
    	
    	try {
			twitter4j.AccountSettings userSettings = userAccount.updateAccountSettings(null, null, null, null, null, lang);
			response = XmlFunctions.GenerateUserLangSettingXml(userSettings);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get the name of the authenticated user
     * @return
     */
    @GET @Path("/profile/name")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserName() {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.showUser(userAccount.getId());
			response = XmlFunctions.GenerateUserNameXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Set the name of the authenticated user
     * @param user_name
     * @return
     */
    @PUT @Path("/profile/name")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_XML)
    public String SetUserName(@FormParam("user_name") String userName) {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.updateProfile(userName, null, null, null);
			response = XmlFunctions.GenerateUserNameXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get the custom URL of the authenticated user
     * @return
     */
    @GET @Path("/profile/url")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserUrl() {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.showUser(userAccount.getId());
			response = XmlFunctions.GenerateUserUrlXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Set the custom URL of the authenticated user
     * @param url is the new URL
     * @return
     */
    @PUT @Path("/profile/url")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_XML)
    public String SetUserUrl(@FormParam("url") String url) {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.updateProfile(null, url, null, null);
			response = XmlFunctions.GenerateUserUrlXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get the description of the authenticated user
     * @return
     */
    @GET @Path("/profile/description")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserDescription() {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.showUser(userAccount.getId());
			response = XmlFunctions.GenerateUserDescriptionXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Set the description of the authenticated user
     * @param description is the new description
     * @return
     */
    @PUT @Path("/profile/description")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_XML)
    public String SetUserDescription(@FormParam("description") String description) {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.updateProfile(null, null, null, description);
			response = XmlFunctions.GenerateUserDescriptionXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get the profile picture URL of the authenticated user
     * @return
     */
    @GET @Path("/profile/profile_picture")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserAvatar() {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.showUser(userAccount.getId());
			response = XmlFunctions.GenerateUserProfilePictureUrlXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Set the profile picture of the authenticated user
     * @param newProfileImagePathname is the new profile image pathname
     * @return
     */
    @PUT @Path("/profile/profile_picture")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_XML)
    public String SetUserAvatar(@FormParam("profile_image_pathname") String newProfileImagePathname) {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
    		File image = new File(newProfileImagePathname);
			twitter4j.User user = userAccount.updateProfileImage(image);
			response = XmlFunctions.GenerateUserProfilePictureUrlXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get the location of the authenticated user
     * @return
     */
    @GET @Path("/profile/location")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserLocation() {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.showUser(userAccount.getId());
			response = XmlFunctions.GenerateUserLocationXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Set the location of the authenticated user
     * @param newLocation is the new location
     * @return
     */
    @PUT @Path("/profile/location")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_XML)
    public String SetUserLocation(@FormParam("new_location") String newLocation) {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User user = userAccount.updateProfile(null, null, newLocation, null);
			response = XmlFunctions.GenerateUserLocationXml(user);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get the list of user following the authenticated user
     * @param cursor
     * @return
     */
    @GET @Path("/followers/list")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserFollowers(@QueryParam("cursor") long cursor) {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.PagableResponseList<twitter4j.User> followerslist = userAccount.getFollowersList(userAccount.getId(), cursor);
			response = XmlFunctions.GenerateUserFollowersListXml(followerslist);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Get the list of friends of the authenticated user
     * @param cursor
     * @return
     */
    @GET @Path("/friends/list")
    @Produces(MediaType.APPLICATION_XML)
    public String GetUserFriends(@QueryParam("cursor") long cursor) {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.PagableResponseList<twitter4j.User> followerslist = userAccount.getFriendsList(userAccount.getId(), cursor);
			response = XmlFunctions.GenerateUserFriendsListXml(followerslist);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Add a new user to the friendslist of the authenticated user
     * @param userToAdd
     * @param follow
     * @return
     */
    @POST @Path("/friendship/{screen_name}")
    @Produces(MediaType.APPLICATION_XML)
    public String CreateFriendship(@PathParam("screen_name") String userToAdd, @DefaultValue("false") @QueryParam("follow") boolean follow) {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User newFriend = userAccount.createFriendship(userToAdd, follow);
			response = XmlFunctions.GenerateUserFriendXml(newFriend);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
     * Remove a user from the authenticated user friendslist
     * @param userToDelete screen name of the user to delete from the friendslist
     * @return
     */
    @DELETE @Path("/friendship/{screen_name}")
    @Produces(MediaType.APPLICATION_XML)
    public String DeleteFriendship(@PathParam("screen_name") String userToDelete) {
        
    	UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;

    	try {
			twitter4j.User newFriend = userAccount.destroyFriendship(userToDelete);
			response = XmlFunctions.GenerateUserFriendXml(newFriend);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return response;
    }
    
    /**
	 * Get the most recent tweets posted by the authenticated user
	 * 
	 * @param screenName
	 *            
	 * @param count
	 *            is the number of tweet to return
	 * @return
	 */
	@GET
	@Path("/time_line")
	@Produces(MediaType.APPLICATION_XML)
	public String GetTimeLine(@DefaultValue("10") @QueryParam("count") long count) {
		
		UserAuthentification client = new UserAuthentification();
    	Twitter userAccount = client.GetTwitterAccount();
    	String response = null;
    	
    	try {
    		int n = 2;
			ResponseList<Status> Liststat = userAccount.getUserTimeline();
			response = XmlFunctions.GenerateTweetsListInfo(Liststat,n);
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response;
	}
}