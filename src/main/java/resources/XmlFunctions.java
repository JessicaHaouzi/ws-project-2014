package resources;

import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import twitter4j.Place;
import twitter4j.ResponseList;
import twitter4j.Status;

public class XmlFunctions {
	
	public static String GenerateUserInfoXml(twitter4j.User user) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			// Profile elements
			Element profile = doc.createElement("profile");
			doc.appendChild(profile);
	 
			// User id
			Element userID = doc.createElement("id");
			userID.appendChild(doc.createTextNode(String.valueOf(user.getId())));
			profile.appendChild(userID);
			
			// User full name
			Element userName = doc.createElement("name");
			userName.appendChild(doc.createTextNode(user.getName()));
			profile.appendChild(userName);
			
			// User Screen name
			Element userScreenName = doc.createElement("screen_name");
			userScreenName.appendChild(doc.createTextNode(user.getScreenName()));
			profile.appendChild(userScreenName);
			
			// User creation
			Element userCreation = doc.createElement("created_at");
			userCreation.appendChild(doc.createTextNode(user.getCreatedAt().toString()));
			profile.appendChild(userCreation);
			
			// User profile image url
			Element profileImage = doc.createElement("profile_image_url");
			profileImage.appendChild(doc.createTextNode(user.getProfileImageURL()));
			profile.appendChild(profileImage);
			
			// User background image url
			Element backgroundImage = doc.createElement("profile_background_image_url");
			backgroundImage.appendChild(doc.createTextNode(user.getProfileBackgroundImageURL()));
			profile.appendChild(backgroundImage);
			
			// User description
			Element userDescription = doc.createElement("description");
			userDescription.appendChild(doc.createTextNode(user.getDescription()));
			profile.appendChild(userDescription);
			
			// User custom url
			if(user.getURL() != null) {
				Element userURL = doc.createElement("url");
				userURL.appendChild(doc.createTextNode(user.getURL()));
				profile.appendChild(userURL);
			}
			
			// User location
			Element location = doc.createElement("location");
			location.appendChild(doc.createTextNode(user.getLocation()));
			profile.appendChild(location);
			
			// User time zone
			if(user.getTimeZone() != null) {
				Element timeZone = doc.createElement("time_zone");
				timeZone.appendChild(doc.createTextNode(user.getTimeZone()));
				profile.appendChild(timeZone);
			}

			// User lang
			Element lang = doc.createElement("lang");
			lang.appendChild(doc.createTextNode(user.getLang()));
			profile.appendChild(lang);

			// User listed count
			Element listedCount = doc.createElement("listed_count");
			listedCount.appendChild(doc.createTextNode(String.valueOf(user.getListedCount())));
			profile.appendChild(listedCount);
			
			// User statuses count
			Element statusesCount = doc.createElement("statuses_count");
			statusesCount.appendChild(doc.createTextNode(String.valueOf(user.getStatusesCount())));
			profile.appendChild(statusesCount);
			
			// User friends count
			Element friendsCount = doc.createElement("friends_count");
			friendsCount.appendChild(doc.createTextNode(String.valueOf(user.getFriendsCount())));
			profile.appendChild(friendsCount);
			
			// User followers count
			Element followersCount = doc.createElement("followers_count");
			followersCount.appendChild(doc.createTextNode(String.valueOf(user.getFollowersCount())));
			profile.appendChild(followersCount);
			
			// User favorite count
			Element favoritesCount = doc.createElement("favourites_count");
			favoritesCount.appendChild(doc.createTextNode(String.valueOf(user.getFavouritesCount())));
			profile.appendChild(favoritesCount);
			
			// HATEOAS
			Element link1 = doc.createElement("link");
			profile.appendChild(link1);
			link1.setAttribute("href", "/users/settings");
			link1.setAttribute("rel", "show settings");
			
			Element link2 = doc.createElement("link");
			profile.appendChild(link2);
			link2.setAttribute("href", "/users/time_line");
			link2.setAttribute("rel", "show time line");

			Element link3 = doc.createElement("link");
			profile.appendChild(link3);
			link3.setAttribute("href", "/tweets/favorites");
			link3.setAttribute("rel", "show favorites tweets");
			
			Element link4 = doc.createElement("link");
			profile.appendChild(link4);
			link4.setAttribute("href", "/users/profile/name");
			link4.setAttribute("rel", "modify name");
			
			Element link5 = doc.createElement("link");
			profile.appendChild(link5);
			link5.setAttribute("href", "/users/profile/url");
			link5.setAttribute("rel", "modify custom url");
			
			Element link6 = doc.createElement("link");
			profile.appendChild(link6);
			link6.setAttribute("href", "/users/profile/description");
			link6.setAttribute("rel", "modify description");
			
			Element link7 = doc.createElement("link");
			profile.appendChild(link7);
			link7.setAttribute("href", "/users/profile/picture_url");
			link7.setAttribute("rel", "modify profile picture url");
			
			Element link8 = doc.createElement("link");
			profile.appendChild(link8);
			link8.setAttribute("href", "/users/profile/location");
			link8.setAttribute("rel", "modify default location");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserSettingXml(twitter4j.AccountSettings userSettings) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			// Settings elements
			Element settings = doc.createElement("settings");
			doc.appendChild(settings);
			
			// User language
			Element lang = doc.createElement("language");
			lang.appendChild(doc.createTextNode(userSettings.getLanguage()));
			settings.appendChild(lang);
			
			// User time zone
			Element timeZone = doc.createElement("time_zone");
			settings.appendChild(timeZone);
			
				// time zone name
			Element timeZoneName = doc.createElement("name");
			timeZoneName.appendChild(doc.createTextNode(userSettings.getTimeZone().getName()));
			timeZone.appendChild(timeZoneName);
			
				// tz info name
			Element timeZoneInfoName = doc.createElement("tzinfo_name");
			timeZoneInfoName.appendChild(doc.createTextNode(userSettings.getTimeZone().tzinfoName()));
			timeZone.appendChild(timeZoneInfoName);
			
				// utc offset
			Element timeZoneUtcOffset = doc.createElement("utc_offset");
			timeZoneUtcOffset.appendChild(doc.createTextNode(String.valueOf(userSettings.getTimeZone().utcOffset())));
			timeZone.appendChild(timeZoneUtcOffset);
			
			// Trend locations
			Element trendLocations = doc.createElement("trend_locations");
			settings.appendChild(trendLocations);

			for (int i=0; i<userSettings.getTrendLocations().length; i++) {
				
				Element trendLocation = doc.createElement("trend_locations");
				trendLocation.setAttribute("id", String.valueOf(i));
				trendLocations.appendChild(trendLocation);
				
				// Country
				Element country = doc.createElement("country");
				country.appendChild(doc.createTextNode(userSettings.getTrendLocations()[i].getCountryName()));
				trendLocation.appendChild(country);
				
				// Country code
				Element countryCode = doc.createElement("countryCode");
				countryCode.appendChild(doc.createTextNode(userSettings.getTrendLocations()[i].getCountryCode()));
				trendLocation.appendChild(countryCode);
				
				// Name
				Element locationName = doc.createElement("name");
				locationName.appendChild(doc.createTextNode(userSettings.getTrendLocations()[i].getName()));
				trendLocation.appendChild(locationName);
				
				// Place type
				Element placeType = doc.createElement("placeType");
				trendLocation.appendChild(placeType);
				
					// Code
				Element placeTypeCode = doc.createElement("code");
				placeTypeCode.appendChild(doc.createTextNode(String.valueOf(userSettings.getTrendLocations()[i].getPlaceCode())));
				placeType.appendChild(placeTypeCode);
				
					// Name
				Element placeTypeName = doc.createElement("name");
				placeTypeName.appendChild(doc.createTextNode(userSettings.getTrendLocations()[i].getPlaceName()));
				placeType.appendChild(placeTypeName);
				
				// URL
				Element url = doc.createElement("url");
				url.appendChild(doc.createTextNode(userSettings.getTrendLocations()[i].getURL()));
				trendLocation.appendChild(url);
				
				// WOEID
				Element woeid = doc.createElement("url");
				woeid.appendChild(doc.createTextNode(String.valueOf(userSettings.getTrendLocations()[i].getWoeid())));
				trendLocation.appendChild(woeid);
			}
			
			// HATEOAS
			Element link1 = doc.createElement("link");
			settings.appendChild(link1);
			link1.setAttribute("href", "/users/settings/time_zone");
			link1.setAttribute("rel", "modify time zone");
			
			Element link2 = doc.createElement("link");
			settings.appendChild(link2);
			link2.setAttribute("href", "/users/settings/lang");
			link2.setAttribute("rel", "modify lang");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserTimeZoneSettingXml(twitter4j.AccountSettings userSettings) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			// User time zone
			Element timeZone = doc.createElement("time_zone");
			doc.appendChild(timeZone);
			
				// time zone name
			Element timeZoneName = doc.createElement("name");
			timeZoneName.appendChild(doc.createTextNode(userSettings.getTimeZone().getName()));
			timeZone.appendChild(timeZoneName);
			
				// tz info name
			Element timeZoneInfoName = doc.createElement("tzinfo_name");
			timeZoneInfoName.appendChild(doc.createTextNode(userSettings.getTimeZone().tzinfoName()));
			timeZone.appendChild(timeZoneInfoName);
			
				// utc offset
			Element timeZoneUtcOffset = doc.createElement("utc_offset");
			timeZoneUtcOffset.appendChild(doc.createTextNode(String.valueOf(userSettings.getTimeZone().utcOffset())));
			timeZone.appendChild(timeZoneUtcOffset);
			
			Element link1 = doc.createElement("link");
			timeZone.appendChild(link1);
			link1.setAttribute("href", "/users/settings");
			link1.setAttribute("rel", "show settings");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserLangSettingXml(twitter4j.AccountSettings userSettings) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element data = doc.createElement("data");
			doc.appendChild(data);
			
			// User language
			Element lang = doc.createElement("language");
			lang.appendChild(doc.createTextNode(userSettings.getLanguage()));
			data.appendChild(lang);
			
			Element link1 = doc.createElement("link");
			data.appendChild(link1);
			link1.setAttribute("href", "/users/settings");
			link1.setAttribute("rel", "show settings");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserNameXml(twitter4j.User user) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element data = doc.createElement("data");
			doc.appendChild(data);
			
			// User full name
			Element userName = doc.createElement("name");
			userName.appendChild(doc.createTextNode(user.getName()));
			data.appendChild(userName);
			
			Element link1 = doc.createElement("link");
			data.appendChild(link1);
			link1.setAttribute("href", "/users/settings");
			link1.setAttribute("rel", "show settings");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserUrlXml(twitter4j.User user) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element data = doc.createElement("data");
			doc.appendChild(data);
			
			// User custom url
			Element userURL = doc.createElement("url");
			userURL.appendChild(doc.createTextNode(user.getURL()));
			data.appendChild(userURL);
			
			Element link1 = doc.createElement("link");
			data.appendChild(link1);
			link1.setAttribute("href", "/users/settings");
			link1.setAttribute("rel", "show settings");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserDescriptionXml(twitter4j.User user) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element data = doc.createElement("data");
			doc.appendChild(data);
			
			// User description
			Element userDescription = doc.createElement("description");
			userDescription.appendChild(doc.createTextNode(user.getDescription()));
			data.appendChild(userDescription);
			
			Element link1 = doc.createElement("link");
			data.appendChild(link1);
			link1.setAttribute("href", "/users/settings");
			link1.setAttribute("rel", "show settings");
			
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserProfilePictureUrlXml(twitter4j.User user) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element data = doc.createElement("data");
			doc.appendChild(data);
			
			// User profile image url
			Element profileImage = doc.createElement("profile_image_url");
			profileImage.appendChild(doc.createTextNode(user.getProfileImageURL()));
			data.appendChild(profileImage);
			
			Element link1 = doc.createElement("link");
			data.appendChild(link1);
			link1.setAttribute("href", "/users/settings");
			link1.setAttribute("rel", "show settings");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserLocationXml(twitter4j.User user) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element data = doc.createElement("data");
			doc.appendChild(data);
			
			// User location
			Element location = doc.createElement("location");
			location.appendChild(doc.createTextNode(user.getLocation()));
			data.appendChild(location);
			
			Element link1 = doc.createElement("link");
			data.appendChild(link1);
			link1.setAttribute("href", "/users/settings");
			link1.setAttribute("rel", "show settings");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserLocationDetailledXml(Place place) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
	 
			// Location elements
			Element rootElement = doc.createElement("location");
			doc.appendChild(rootElement);
						
			// Location country
			Element country = doc.createElement("country");
			country.appendChild(doc.createTextNode(place.getCountry()));
			doc.appendChild(country);
			
			// Location country code
			Element countrycode = doc.createElement("countrycode");
			countrycode.appendChild(doc.createTextNode(place.getCountryCode()));
			doc.appendChild(countrycode);
			
			// Location fullname
			Element fullname = doc.createElement("fullname");
			fullname.appendChild(doc.createTextNode(place.getCountryCode()));
			doc.appendChild(fullname);
						
			// Location name
			Element name = doc.createElement("name");
			name.appendChild(doc.createTextNode(place.getCountryCode()));
			doc.appendChild(name);
			
			// Location place type
			Element placetype = doc.createElement("placetype");
			placetype.appendChild(doc.createTextNode(place.getCountryCode()));
			doc.appendChild(placetype);
			
			// Location URL
			Element url = doc.createElement("url");
			url.appendChild(doc.createTextNode(place.getCountryCode()));
			doc.appendChild(url);

			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserFollowersListXml(twitter4j.PagableResponseList<twitter4j.User> followerslist) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			// Data root element
			Element rootElement = doc.createElement("data");
			doc.appendChild(rootElement);
			
			// Followers elements
			Element followers = doc.createElement("followers");
			rootElement.appendChild(followers);
			
			for (int i = 0; i < followerslist.size(); i++) {
				Element follower = doc.createElement("follower");
				follower.setAttribute("id", String.valueOf(i));
				followers.appendChild(follower);
		 
				// User id
				Element userID = doc.createElement("id");
				userID.appendChild(doc.createTextNode(String.valueOf(followerslist.get(i).getId())));
				follower.appendChild(userID);
				
				// User full name
				Element userName = doc.createElement("name");
				userName.appendChild(doc.createTextNode(followerslist.get(i).getName()));
				follower.appendChild(userName);
				
				// User Screen name
				Element userScreenName = doc.createElement("screen_name");
				userScreenName.appendChild(doc.createTextNode(followerslist.get(i).getScreenName()));
				follower.appendChild(userScreenName);
				
				// User creation
				Element userCreation = doc.createElement("created_at");
				userCreation.appendChild(doc.createTextNode(followerslist.get(i).getCreatedAt().toString()));
				follower.appendChild(userCreation);
				
				// User profile image url
				Element profileImage = doc.createElement("profile_image_url");
				profileImage.appendChild(doc.createTextNode(followerslist.get(i).getProfileImageURL()));
				follower.appendChild(profileImage);
				
				// User background image url
				Element backgroundImage = doc.createElement("profile_background_image_url");
				backgroundImage.appendChild(doc.createTextNode(followerslist.get(i).getProfileBackgroundImageURL()));
				follower.appendChild(backgroundImage);
				
				// User description
				Element userDescription = doc.createElement("description");
				userDescription.appendChild(doc.createTextNode(followerslist.get(i).getDescription()));
				follower.appendChild(userDescription);
				
				// User custom url
				Element userURL = doc.createElement("url");
				userURL.appendChild(doc.createTextNode(followerslist.get(i).getURL()));
				follower.appendChild(userURL);
				
				// User location
				Element location = doc.createElement("location");
				location.appendChild(doc.createTextNode(followerslist.get(i).getLocation()));
				follower.appendChild(location);
				
				// User time zone 
				Element timeZone = doc.createElement("time_zone");
				timeZone.appendChild(doc.createTextNode(followerslist.get(i).getTimeZone()));
				follower.appendChild(timeZone);
				
				// User lang
				Element lang = doc.createElement("lang");
				lang.appendChild(doc.createTextNode(followerslist.get(i).getLang()));
				follower.appendChild(lang);
			}
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserFriendsListXml(twitter4j.PagableResponseList<twitter4j.User> friendslist) {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			// Data root element
			Element rootElement = doc.createElement("data");
			doc.appendChild(rootElement);
			
			// Friends elements
			Element friends = doc.createElement("friends");
			rootElement.appendChild(friends);
			
			for (int i = 0; i < friendslist.size(); i++) {
				Element friend = doc.createElement("friend");
				friend.setAttribute("id", String.valueOf(i));
				friends.appendChild(friend);
		 
				// User id
				Element userID = doc.createElement("id");
				userID.appendChild(doc.createTextNode(String.valueOf(friendslist.get(i).getId())));
				friend.appendChild(userID);
				
				// User full name
				Element userName = doc.createElement("name");
				userName.appendChild(doc.createTextNode(friendslist.get(i).getName()));
				friend.appendChild(userName);
				
				// User Screen name
				Element userScreenName = doc.createElement("screen_name");
				userScreenName.appendChild(doc.createTextNode(friendslist.get(i).getScreenName()));
				friend.appendChild(userScreenName);
				
				// User creation
				Element userCreation = doc.createElement("created_at");
				userCreation.appendChild(doc.createTextNode(friendslist.get(i).getCreatedAt().toString()));
				friend.appendChild(userCreation);
				
				// User profile image url
				Element profileImage = doc.createElement("profile_image_url");
				profileImage.appendChild(doc.createTextNode(friendslist.get(i).getProfileImageURL()));
				friend.appendChild(profileImage);
				
				// User background image url
				Element backgroundImage = doc.createElement("profile_background_image_url");
				backgroundImage.appendChild(doc.createTextNode(friendslist.get(i).getProfileBackgroundImageURL()));
				friend.appendChild(backgroundImage);
				
				// User description
				Element userDescription = doc.createElement("description");
				userDescription.appendChild(doc.createTextNode(friendslist.get(i).getDescription()));
				friend.appendChild(userDescription);
				
				// User custom url
				Element userURL = doc.createElement("url");
				userURL.appendChild(doc.createTextNode(friendslist.get(i).getURL()));
				friend.appendChild(userURL);
				
				// User location
				Element location = doc.createElement("location");
				location.appendChild(doc.createTextNode(friendslist.get(i).getLocation()));
				friend.appendChild(location);
				
				// User time zone 
				Element timeZone = doc.createElement("time_zone");
				timeZone.appendChild(doc.createTextNode(friendslist.get(i).getTimeZone()));
				friend.appendChild(timeZone);
				
				// User lang
				Element lang = doc.createElement("lang");
				lang.appendChild(doc.createTextNode(friendslist.get(i).getLang()));
				friend.appendChild(lang);
			}
			
			// HATEOAS
			Element link1 = doc.createElement("link");
			friends.appendChild(link1);
			link1.setAttribute("href", "/friendship/{screen_name}");
			link1.setAttribute("rel", "add friend");
			
			Element link2 = doc.createElement("link");
			friends.appendChild(link2);
			link2.setAttribute("href", "/friendship/{screen_name}");
			link2.setAttribute("rel", "remove friend");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateUserFriendXml(twitter4j.User newFriend)  {
		
		String xml = null;
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element friend = doc.createElement("friend");
			doc.appendChild(friend);
			
			// User id
			Element userID = doc.createElement("id");
			userID.appendChild(doc.createTextNode(String.valueOf(newFriend.getId())));
			friend.appendChild(userID);
			
			// User full name
			Element userName = doc.createElement("name");
			userName.appendChild(doc.createTextNode(newFriend.getName()));
			friend.appendChild(userName);
			
			// User Screen name
			Element userScreenName = doc.createElement("screen_name");
			userScreenName.appendChild(doc.createTextNode(newFriend.getScreenName()));
			friend.appendChild(userScreenName);
			
			// User creation
			Element userCreation = doc.createElement("created_at");
			userCreation.appendChild(doc.createTextNode(newFriend.getCreatedAt().toString()));
			friend.appendChild(userCreation);
			
			// User profile image url
			Element profileImage = doc.createElement("profile_image_url");
			profileImage.appendChild(doc.createTextNode(newFriend.getProfileImageURL()));
			friend.appendChild(profileImage);
			
			// User background image url
			Element backgroundImage = doc.createElement("profile_background_image_url");
			backgroundImage.appendChild(doc.createTextNode(newFriend.getProfileBackgroundImageURL()));
			friend.appendChild(backgroundImage);
			
			// User description
			Element userDescription = doc.createElement("description");
			userDescription.appendChild(doc.createTextNode(newFriend.getDescription()));
			friend.appendChild(userDescription);
			
			// User custom url
			Element userURL = doc.createElement("url");
			userURL.appendChild(doc.createTextNode(newFriend.getURL()));
			friend.appendChild(userURL);
			
			// User location
			Element location = doc.createElement("location");
			location.appendChild(doc.createTextNode(newFriend.getLocation()));
			friend.appendChild(location);
			
			// User time zone 
			Element timeZone = doc.createElement("time_zone");
			timeZone.appendChild(doc.createTextNode(newFriend.getTimeZone()));
			friend.appendChild(timeZone);
			
			// User lang
			Element lang = doc.createElement("lang");
			lang.appendChild(doc.createTextNode(newFriend.getLang()));
			friend.appendChild(lang);
			
			// HATEOAS
			Element link1 = doc.createElement("link");
			friend.appendChild(link1);
			link1.setAttribute("href", "/friends/list");
			link1.setAttribute("rel", "show friends list");
	 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
	 
			xml = output;
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		return xml;
	}
	
	public static String GenerateTweetInfo(Status status, int n) {
		
		String xml = null;
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();

			Element stat = doc.createElement("tweet");
			doc.appendChild(stat);

			// Tweet id
			Element tweet = doc.createElement("tweet_id");
			tweet.appendChild(doc.createTextNode(String.valueOf(status.getId())));
			stat.appendChild(tweet);
			
			// Author
			Element tweetAuthor = doc.createElement("tweet_author");
			tweetAuthor.appendChild(doc.createTextNode(status.getUser().getScreenName()));
			stat.appendChild(tweetAuthor);

			// Tweet content
			Element texte = doc.createElement("tweet_content");
			texte.appendChild(doc.createTextNode(String.valueOf(status.getText())));
			stat.appendChild(texte);
			
			// Creation date
			Element creationDate = doc.createElement("created_at");
			creationDate.appendChild(doc.createTextNode(status.getCreatedAt().toString()));
			stat.appendChild(creationDate);
			
			// Location
			Element location = doc.createElement("created_at");
			location.appendChild(doc.createTextNode(status.getPlace().getFullName()));
			stat.appendChild(location);
			
			// HATEOAS
			if(n == 1){
				Element link1 = doc.createElement("link");
				stat.appendChild(link1);
				link1.setAttribute("href", "/tweets/favorite/create");
				link1.setAttribute("rel", "add tweet to favorites");
				
			} else if(n == 2){
				Element link2 = doc.createElement("link");
				stat.appendChild(link2);
				link2.setAttribute("href", "/tweets/time_line");
				link2.setAttribute("rel", "show time line");
				
			} else if(n == 3){
				Element link3 = doc.createElement("link");
				stat.appendChild(link3);
				link3.setAttribute("href", "/tweets/favorites/destroy");
				link3.setAttribute("rel", "remove tweet from favorites");
			
			} else if(n == 4){
				Element link4 = doc.createElement("link");
				stat.appendChild(link4);
				link4.setAttribute("href", "/tweets/favorites");
				link4.setAttribute("rel", "show favorites");
				
			} else{
				Element link3 = doc.createElement("link");
				stat.appendChild(link3);
				link3.setAttribute("href", "/tweets/favorites/destroy");
				link3.setAttribute("rel", "remove tweet from favorites");
				
				Element link4 = doc.createElement("link");
				stat.appendChild(link4);
				link4.setAttribute("href", "/tweets/favorites");
				link4.setAttribute("rel", "show favorites");
				
			}
			
			
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");

			xml = output;

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
		return xml;
	}

	public static String GenerateTweetsListInfo(ResponseList<Status> status, int n) {
		String xml = null;
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();

			// Data root element
			Element rootElement = doc.createElement("data");
			doc.appendChild(rootElement);

			// Status elements
			Element status2 = doc.createElement("tweets");
			rootElement.appendChild(status2);

			for (int i = 0; i < status.size(); i++) {
				
				Element stat = doc.createElement("tweet");
				stat.setAttribute("id", String.valueOf(i));
				status2.appendChild(stat);

				// Tweet id
				Element tweet = doc.createElement("tweet_id");
				tweet.appendChild(doc.createTextNode(String.valueOf(status.get(i).getId())));
				stat.appendChild(tweet);
				
				// Author
				Element tweetAuthor = doc.createElement("tweet_author");
				tweetAuthor.appendChild(doc.createTextNode(status.get(i).getUser().getScreenName()));
				stat.appendChild(tweetAuthor);

				// Tweet content
				Element texte = doc.createElement("tweet_content");
				texte.appendChild(doc.createTextNode(String.valueOf(status.get(i).getText())));
				stat.appendChild(texte);
				
				// Creation date
				Element creationDate = doc.createElement("created_at");
				creationDate.appendChild(doc.createTextNode(status.get(i).getCreatedAt().toString()));
				stat.appendChild(creationDate);
				
				// Location
				Element location = doc.createElement("created_at");
				location.appendChild(doc.createTextNode(status.get(i).getPlace().getFullName()));
				stat.appendChild(location);

			}
			
			// HATEOAS
			
			if(n == 1){
				Element link1 = doc.createElement("link");
				status2.appendChild(link1);
				link1.setAttribute("href", "/tweets/favorites/destroy");
				link1.setAttribute("rel", "remove tweet from favorites");
			
			} else{
				Element link2 = doc.createElement("link");
				status2.appendChild(link2);
				link2.setAttribute("href", "/tweets/create");
				link2.setAttribute("rel", "create tweet");
			
				Element link3 = doc.createElement("link");
				status2.appendChild(link3);
				link3.setAttribute("href", "/tweets/destroy");
				link3.setAttribute("rel", "delete tweet");
			}

			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");

			xml = output;

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
		return xml;
	}
	
	public static String GenerateTweetsListInfo(List<Status> status) {
		String xml = null;
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();

			// Data root element
			Element rootElement = doc.createElement("data");
			doc.appendChild(rootElement);

			// Status elements
			Element status2 = doc.createElement("tweets");
			rootElement.appendChild(status2);

			for (int i = 0; i < status.size(); i++) {
				
				Element stat = doc.createElement("tweet");
				stat.setAttribute("id", String.valueOf(i));
				status2.appendChild(stat);

				// Tweet id
				Element tweet = doc.createElement("tweet_id");
				tweet.appendChild(doc.createTextNode(String.valueOf(status.get(i).getId())));
				stat.appendChild(tweet);
				
				// Author
				Element tweetAuthor = doc.createElement("tweet_author");
				tweetAuthor.appendChild(doc.createTextNode(status.get(i).getUser().getScreenName()));
				stat.appendChild(tweetAuthor);

				// Tweet content
				Element texte = doc.createElement("tweet_content");
				texte.appendChild(doc.createTextNode(String.valueOf(status.get(i).getText())));
				stat.appendChild(texte);
				
				// Creation date
				Element creationDate = doc.createElement("created_at");
				creationDate.appendChild(doc.createTextNode(status.get(i).getCreatedAt().toString()));
				stat.appendChild(creationDate);
				
				// Location
				Element location = doc.createElement("created_at");
				location.appendChild(doc.createTextNode(status.get(i).getPlace().getFullName()));
				stat.appendChild(location);

			}
			
			// HATEOAS
			Element link1 = doc.createElement("link");
			status2.appendChild(link1);
			link1.setAttribute("href", "/tweets/favorite/create");
			link1.setAttribute("rel", "add tweet to favorites");

			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString().replaceAll("\n|\r", "");

			xml = output;

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
		return xml;
	}
}
